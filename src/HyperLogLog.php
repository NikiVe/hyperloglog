<?php

namespace HyperLogLog;

class HyperLogLog
{
    protected array $arRegister = [
        0 => 0,
        1 => 0,
        2 => 0,
        3 => 0
    ];

    protected int $size;
    protected int $mask; // Маска для определения регистров
    protected int $shift; // Сдвиг для основной части

    public function __construct(int $size) {
        if (($size & 1) === 1) {
            throw new \Exception();
        }
        $this->size = $size;

        for ($i = 0; $i < $size; $i++) {
            $this->arRegister[$i] = 0;
        }

        $this->mask = $size - 1;
        $this->shift = log($size, 2);
    }

    public function getAlpha($m) {
        $r = 0;
        switch ($m) {
            case 16:
                $r = 0.673;
                break;
            case 32:
                $r = 0.697;
                break;
            case 64:
                $r = 0.709;
                break;
        }


        return $r / (1 + 1.079 / $this->size);
    }

    public static function rank(int $hash, int $max = 32) : int {
        $r = 1;
        while (($hash & 1) === 0 && $r <= $max) {
            $r++;
            $hash >>= 1;
        }
        return $r;
    }

    public function split(int $hash) : array {
        $result[] = $hash & $this->mask;
        $result[] = $hash >> $this->shift;
        return $result;
    }

    public function add(string $item) {
        $hash = hexdec(hash('crc32b', $item));
        $split = $this->split($hash);
        $this->addToRegister($split[0], $split[1]);
    }

    protected function addToRegister($num, $hash) {
        $r = self::rank($hash);
        if ($r > $this->arRegister[$num]) {
            $this->arRegister[$num] = $r;
        }
    }

    public function getCount() :int {
        $z = 0;

        foreach ($this->arRegister as $register) {
            $z += (1.0 / pow(2, $register));
        }

        return (1 / $z) * $this->getAlpha(32) * pow($this->size, 2);
    }
}